import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from './AuthenticationService';

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective {

  private authorities: string[];

  constructor(
    private authenticationService: AuthenticationService,
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef
  ) {}

  @Input()
  set hasPermission(value: string | string[]) {
    this.authorities = typeof value === 'string' ? [value] : value;
    this.updateView();
    // Get notified each time authentication state changes.
    // this.authenticationService.getAuthenticationState().subscribe(identity => this.updateView());
  }

  private updateView(): void {
    const hasFeature = this.authenticationService.hasOneFeature(this.authorities);
    this.viewContainerRef.clear();
    if (hasFeature) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    }
  }
}
