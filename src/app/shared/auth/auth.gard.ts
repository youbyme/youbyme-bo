import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { User } from '../../models/user.model';
import { AuthenticationService } from './AuthenticationService';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // not logged in so redirect to login page with the return url
    if (!this.authenticationService.isAuthenticated()) {
      this.router.navigate(['login'], { queryParams: { returnUrl: state.url }}).then(() => {
      });
      window.alert('Authentication not recognized. Please log in');
      return false;
    } else {
      if (!this.authenticationService.hasOneFeature(this.getCanActivateFeatures(route))) {
        window.alert('You don\'t have the required authorizations');
        return false;
      }
      return true;
    }
  }

  capitalize(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  getCanActivateFeatures(route) {
    const features = [];
    switch (route.component.name) {
      case 'HomeComponent':
        features.push('loggued');
        break;
      default:
        // feature are formatted : Read|Create|Update|Delete + Entities : ex -> ReadUtilisateurs
        let action = route.params['action'];
        action = action ? action : 'Read';
        const crud = route.params['crud'];
        if (action && crud) {
          features.push(this.capitalize(action) + this.capitalize(crud));
        }
        break;
    }
    return features;
  }
}
