import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../models/user.model';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ErrorService } from '../services/error.service';

@Injectable(
  { providedIn: 'root' }
)
export class AuthenticationService {

  private authenticationUrl = environment.apiUrl + `/v1/login_check`;
  private lsUserKey = 'currentUser';
  private refreshUrl = environment.apiUrl + '/v1/token/refresh';
  private logguedIn = new BehaviorSubject<boolean>(this.isAuthenticated());

  public authToken;
  public refreshToken;
  public currentUser;


  constructor(
    private http: HttpClient,
    private errorService: ErrorService,
    private router: Router,
    private jwtHelper: JwtHelperService
  ) { }

  login(username: string, password: string) {
    return this.http.post<any>(this.authenticationUrl, { username: username, password: password })
      .pipe(
        catchError(this.errorService.manageError),
        map(tokens => {
          // login successful if there's a jwt token in the response
          if (tokens && tokens.token && tokens['refresh_token']) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            this.setToken(tokens.token);
            this.setRefreshToken(tokens.refresh_token);
          }
          return tokens;
        })
      );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    localStorage.removeItem('refresh_token');
  }

  getConnectedUser(): Observable<User> {
    return this.http.get(environment.apiUrl + '/api/v1/auth/currentuser').pipe(
      catchError(this.errorService.manageError),
      map((user: User) => {
        if (user != null) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUser = user;
        }
        return user;
      })
    );
  }

  isRefreshRequest(request): boolean {
    return request.url === this.refreshUrl;
  }

  refreshLoggedIn() {
    this.logguedIn.next(this.isAuthenticated());
  }

  public isAuthenticated(): boolean {
    return this.getToken() != null && this.getToken() && this.jwtHelper && !this.jwtHelper.isTokenExpired(this.authToken);
  }

  refresh() {
    const body = {
      'refresh_token': this.getRefreshToken()
    };
    return this.http.post(this.refreshUrl, body);
  }

  setCurrentUser(user: User) {
    this.currentUser = user;
    localStorage.setItem(this.lsUserKey, JSON.stringify(this.currentUser));
  }

  getCurrentUser() {
    if (!this.currentUser) {
      this.currentUser = JSON.parse(localStorage.getItem(this.lsUserKey));
    }
    return this.currentUser;
  }

  setToken(token: string) {
    this.authToken = token;
    localStorage.setItem('token', token);
  }

  getToken() {
    if (!this.authToken) {
      this.authToken = localStorage.getItem('token');
    }
    return this.authToken;
  }

  setRefreshToken(token: string) {
    this.refreshToken = token;
    localStorage.setItem('refresh_token', token);
  }

  getRefreshToken() {
    if (!this.refreshToken) {
      this.refreshToken = localStorage.getItem('refresh_token');
    }
    return this.refreshToken;
  }

  hasOneFeature(features: string[]) {
    const currentUser = this.getCurrentUser();
    if (currentUser) {
      for (const feat of features) {
        if (currentUser.features.includes(feat)) {
          return true;
        }
      }
    }
    // return true (authorized) if no features required,
    // false if features required and not found in user
    return features.length === 0;
  }
}
