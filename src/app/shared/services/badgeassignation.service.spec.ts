import { TestBed } from '@angular/core/testing';

import { BadgeassignationService } from './badgeassignation.service';

describe('BadgeassignationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BadgeassignationService = TestBed.get(BadgeassignationService);
    expect(service).toBeTruthy();
  });
});
