import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { ErrorService } from './error.service';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BadgeassignationService extends CommonService {

  constructor(
    errorService: ErrorService,
    http: HttpClient
  ) {
    super(errorService, http, 'badgeAssignations');
  }

  validUserBadge(utilId, badgId) {
    if (!utilId || utilId === '' || !badgId || badgId === '') {
      throw new Error('Need both id');
    }
    return this.http.put(this.getComponentUrl() + '/' + utilId + '/' + badgId, {}).pipe(
      catchError(this.errorService.manageError)
    );
  }
}
