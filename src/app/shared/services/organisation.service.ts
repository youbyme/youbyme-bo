import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { ErrorService } from './error.service';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrganisationService extends CommonService {

  constructor(
    errorService: ErrorService,
    http: HttpClient
  ) {
    super(errorService, http, 'organisations');
  }

  resetPoints(id) {
    if (!id || id === '') {
      throw new Error('No id');
    }
    return this.http.put(this.componentUrl + '/' + id + '/resetPoints', {}).pipe(
      catchError(this.errorService.manageError)
    );
  }
}
