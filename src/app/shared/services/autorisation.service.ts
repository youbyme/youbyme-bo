import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class AutorisationService extends CommonService {

  constructor(
    errorService: ErrorService,
    http: HttpClient
  ) {
    super(errorService, http, 'autorisations');
  }
}
