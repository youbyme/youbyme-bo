import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  manageError(error) {
    let message = 'An error has occured: \n';
    if (error.error && error.error.message) {
      message = error.error.message + '\n';
    }
    if (error.error.errors && error.error.errors instanceof Array) {
      for (const msg of error.error.errors) {
        message += msg + '\n';
      }
    }
    window.alert(message);
    return throwError(error);
  }
}
