import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { ErrorService } from './error.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompetenceService extends CommonService {

  constructor(
    errorService: ErrorService,
    http: HttpClient
  ) {
    super(errorService, http, 'competences');
  }
}
