import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { ErrorService } from './error.service';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService extends CommonService {

  constructor(
    errorService: ErrorService,
    http: HttpClient
  ) {
    super(errorService, http, 'utilisateurs');
  }

  postUsersList(users, codeAnalytics) {
    return this.http.post(this.componentUrl + '/import/' + codeAnalytics, users).pipe(
      catchError(this.errorService.manageError)
    );
  }

  getProgresses(utilId) {
    if (!utilId || utilId === '') {
      throw new Error('No id');
    }
    return this.http.get(this.componentUrl + '/' + utilId + '/progress').pipe(
      catchError(this.errorService.manageError)
    );
  }

  getUserResetHistory() {
    return this.http.get(this.getComponentUrl('historique')).pipe(
      catchError(this.errorService.manageError)
    );
  }
}
