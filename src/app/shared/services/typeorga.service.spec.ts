import { TestBed } from '@angular/core/testing';

import { TypeorgaService } from './typeorga.service';

describe('TypeorgaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeorgaService = TestBed.get(TypeorgaService);
    expect(service).toBeTruthy();
  });
});
