import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Autorisation } from '../../models/autorisation.model';
import { Badge } from '../../models/badge.model';
import { Competence } from '../../models/competence.model';
import { Organisation } from '../../models/organisation.model';
import { Regle } from '../../models/regle.model';
import { TypeOrga } from '../../models/type-orga.model';
import { Vote } from '../../models/vote.model';
import { ErrorService } from './error.service';
import { BadgeAssignation } from '../../models/badge-assignation.model';

export class CommonService {

  protected componentUrl;

  constructor(
    protected errorService: ErrorService,
    protected http: HttpClient,
    protected component: string
  ) {
    this.componentUrl = this.getComponentUrl(component);
  }

  getAll() {
    return this.http.get(this.componentUrl).pipe(
      catchError(this.errorService.manageError),
      map(eltArray => this.mapAllElements(eltArray))
    );
  }

  getSelectList(component) {
    return this.http.get(this.getComponentUrl(component)).pipe(
      catchError(this.errorService.manageError),
      map((eltArray: Array<any>) => {
        eltArray = this.mapAllElements(eltArray);
        let selectList;
        switch (component) {
          case 'utilisateurs':
            selectList = eltArray.map((e) => {
              return {
                'utilId' : e.utilId,
                'utilCivilite' : e.utilCivilite,
                'utilPrenom' : e.utilPrenom,
                'utilNom' : e.utilNom
              };
            });
            break;
          case 'competences':
            selectList = eltArray.map((e) => {
              return {
                'compId': e.compId,
                'compNom': e.compNom
              };
            });
            break;
          case 'typeorgas':
            selectList = eltArray.map((e) => {
              return {
                'typeOrgaId' : e.typeOrgaId,
                'typeOrgaNom' : e.typeOrgaNom
              };
            });
            break;
          case 'regles':
            selectList = eltArray.map((e) => {
              return {
                'reglId' : e.reglId,
                'reglNom' : e.reglNom
              };
            });
            break;
          case 'organisations':
            selectList = eltArray.map((e) => {
              return {
                'orgaId' : e.orgaId,
                'orgaNom' : e.orgaNom,
                'orgaCodeAnalytics' : e.orgaCodeAnalytics
              };
            });
            break;
          case 'roles':
            selectList = eltArray.map((e) => {
              return {
                'roleId': e.roleId,
                'roleNom': e.roleNom
              };
            });
            break;
          case 'autorisations':
            selectList = eltArray.map((e) => {
              return {
                'autoId': e.autoId,
                'autoNom': e.autoNom
              };
            });
            break;
          case 'badges':
            selectList = eltArray.map((e) => {
              return {
                'badgId' : e.badgId,
                'badgNom' : e.badgNom
              };
            });
            break;
          default:
            selectList = eltArray;
        }
        return selectList;
      })
    );
  }

  post(elt) {
    if (elt[this.getEltIdName(this.component)]) {
      throw new Error('Can\'t have id in post request');
    }
    return this.http.post(this.componentUrl, elt).pipe(
      catchError(this.errorService.manageError),
      map(element => this.mapElement(element))
    );
  }

  put(elt) {
    const id = elt[this.getEltIdName(this.component)];
    if (!id) {
      throw new Error('No id');
    }
    return this.http.put(this.componentUrl + '/' + id, elt).pipe(
      catchError(this.errorService.manageError),
      map(element => this.mapElement(element))
    );
  }

  delete(id) {
    if (!id || id === '') {
      throw new Error('No id');
    }
    return this.http.delete(this.componentUrl + '/' + id).pipe(
      catchError(this.errorService.manageError)
    );
  }

  getComponentUrl(component?) {
    if (!component) {
      return this.componentUrl;
    } else {
      return environment.apiUrl + '/api/' + environment.apiVersion + '/' + component.toLowerCase();
    }
  }

  // useless for now but keep it if need later
  mapElement(elt) {
    return elt;
  }

  mapAllElements(eltArray) {
    for (let elt of eltArray) {
      elt = this.mapElement(elt);
    }
    return eltArray;
  }

  getEltIdName(component) {
    switch (component) {
      case 'utilisateurs':
        return 'utilId';
      case 'roles':
        return 'roleId';
      case 'autorisations':
        return 'autoId';
      case 'badges':
        return 'badgId';
      case 'badgeAssignations':
        return 'badgId';
      case 'competences':
        return 'compId';
      case 'organisations':
        return 'orgaId';
      case 'regles':
        return 'reglId';
      case 'typeOrgas':
        return 'typeOrgaId';
      case 'votes':
        return 'voteId';
      case 'voteType' :
        return 'valide';
    }

  }
}
