import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public isVisible;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  isNavbarVisible() {
    return !this.router.url.startsWith('/login') && !this.router.url.startsWith('/logout');
  }

  collapse(menu) {
    menu.classList.remove('show');
    menu.classList.add('hide');
  }
}
