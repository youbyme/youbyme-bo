import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FullCalendarModule } from 'primeng/fullcalendar';
import {
  CalendarModule,
  DialogModule,
  MultiSelectModule,
  DropdownModule,
  KeyFilterModule,
  FileUploadModule,
  PasswordModule
} from 'primeng/primeng';
import { HasPermissionDirective } from './auth/has-permission.directive';

@NgModule({
  declarations: [HasPermissionDirective],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TableModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    TableModule,
    DialogModule,
    MultiSelectModule,
    DropdownModule,
    FileUploadModule,
    FullCalendarModule,
    PasswordModule,
    KeyFilterModule,
    CalendarModule,
    TranslateModule,
    HasPermissionDirective
  ]
})
export class SharedModule { }
