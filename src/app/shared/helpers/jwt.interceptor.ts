import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { AuthenticationService } from '../auth/AuthenticationService';
import { catchError, filter, finalize, switchMap, take } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  private refreshInProgress = false;
  private readonly tokenRefreshedSource = new Subject();
  private readonly tokenRefreshed = this.tokenRefreshedSource.asObservable();
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(
    public authService: AuthenticationService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any> | any> {
    return next.handle(this.setHeaders(request))
      .pipe(
        catchError((error) => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401 && (error.error.message === 'Expired JWT Token' || error.error.message === 'Invalid JWT Token')) {
              // if (error.status === 404) {
              if (this.authService.getToken() && this.authService.getRefreshToken() && !this.authService.isRefreshRequest(request)) {
                return this.callRefresh(request, next);
              } else {
                // Do nothing, the call returns to the catch after this.auth.refresh()
                console.log('NOT CALL REFRESH');

              }
            } else {
              return throwError(error);
            }
          } else {
            return throwError(error);
          }
        })
      );
  }

  private callRefresh(request: HttpRequest<any>, next: HttpHandler) {
    if (!this.refreshInProgress) {
      this.refreshInProgress = true;

      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next(null);

      return this.authService.refresh()
        .pipe(
          switchMap((data: any) => {
            if (data && data['token'] && data['refresh_token']) {
              this.authService.setToken(data.token);
              this.authService.setRefreshToken(data.refresh_token);
              this.authService.refreshLoggedIn();
              this.tokenSubject.next(data);
              return next.handle(this.setHeaders(request));
            }

            return <any>this.authService.logout();
          }),
          catchError(err => {
            return <any>this.authService.logout();
          }),
          finalize(() => {
            this.refreshInProgress = false;
          })
        );
    } else {
      this.refreshInProgress = false;

      return this.tokenSubject
        .pipe(
          filter(token => token != null),
          take(1),
          switchMap(token => {
            return next.handle(this.setHeaders(request));
          }));
    }
  }

  private setHeaders(request: HttpRequest<any>) {
    const headers = {};
    if (this.authService.getToken()) {
      headers['Authorization'] = 'Bearer ' + this.authService.getToken();
    }
    if (!request.headers.has('Content-Type')) {
      headers['Content-Type'] = 'application/json';
    }
    return request.clone({'setHeaders': headers});
  }
}
