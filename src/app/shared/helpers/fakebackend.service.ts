// import { Injectable } from '@angular/core';
// import { environment } from '../../../environments/environment';
// import { User } from '../../models/user.model';
// import { of, throwError } from 'rxjs';
// import { HttpResponse } from '@angular/common/http';
// import { Role } from '../../models/role.model';
// import { Regle } from '../../models/regle.model';
// import { Organisation } from '../../models/organisation.model';
// import { Competence } from '../../models/competence.model';
// import { Vote } from '../../models/vote.model';
// import { Autorisation } from '../../models/autorisation.model';
// import { Badge } from '../../models/badge.model';
// import { TypeOrga } from '../../models/type-orga.model';
// import { CommonService } from '../services/common.service';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class FakebackendService {
//
//   fakeRoutes = [
//     {
//       fakeUrlPattern: /.*\/v1\/autentification\/logout/,
//       method: 'GET'
//     },
//     {
//       fakeUrlPattern: /.*\/v1\/autorisations\/role/,
//       method: 'GET'
//     },
//     {
//       fakeUrlPattern: /.*\/v1\/[a-z]*\/all/,
//       method: 'GET'
//     },
//     {
//       fakeUrlPattern: /.*\/v1\/[a-z]*$/,
//       method: 'GET'
//     },
//     {
//       fakeUrlPattern: /.*\/v1\/[a-z]*$/,
//       method: 'POST'
//     },
//     {
//       fakeUrlPattern: /.*\/v1\/[a-z]*$/,
//       method: 'DELETE'
//     },
//     {
//       fakeUrlPattern: /.*\/v1\/[a-z]*$/,
//       method: 'PUT'
//     }
//   ];
//
//   testUser: User = new User();
//
//   bdd = {
//     users: [],
//     roles: [],
//     autorisations: [],
//     badges: [],
//     competences: [],
//     organisations: [],
//     regles: [],
//     typeorgas: [],
//     votes: [],
//   };
//
//   currentIndexes = {
//     users: 3,
//     roles: 3,
//     autorisations: 3,
//     badges: 3,
//     competences: 3,
//     organisations: 3,
//     regles: 3,
//     typeorgas: 3,
//     votes: 3,
//   };
//
//   eltIdField = {
//     users: 'utilId',
//     roles: 'roleId',
//     autorisations: 'autoId',
//     badges: 'badgId',
//     competences: 'compId',
//     organisations: 'orgaId',
//     regles: 'reglId',
//     typeorgas: 'typeorgaId',
//     votes: 'voteId',
//   };
//
//   constructor(
//     private commonService: CommonService
//   ) {
//     this.testUser.utilEmail = 'test';
//     this.testUser.utilMotDePasse = 'test';
//     this.startBackend();
//   }
//
//   startBackend() {
//     // autorisations
//     this.bdd.autorisations = [];
//     let autorisation1 = new Autorisation();
//     autorisation1.autoId = '11111111111';
//     autorisation1.autoNom = 'autoNom1';
//     autorisation1.autoDescription = 'autoDescription1';
//     let autorisation2 = new Autorisation();
//     autorisation2.autoId = '22222222222';
//     autorisation2.autoNom = 'autoNom2';
//     autorisation2.autoDescription = 'autoDescription2';
//     // roles
//     this.bdd.roles = [];
//     let role1 = new Role();
//     role1.roleId = '11111111111';
//     role1.roleNom = 'roleNom1';
//     role1.roleDescription = 'roleDescription1';
//     let role2 = new Role();
//     role2.roleId = '22222222222';
//     role2.roleNom = 'roleNom2';
//     role2.roleDescription = 'roleDescription2';
//     // badges
//     this.bdd.badges = [];
//     let badge1 = new Badge();
//     badge1.badgId = '11111111111';
//     badge1.badgNom = 'badgNom1';
//     badge1.badgImageUrl = 'badgImageUrl1';
//     badge1.badgDescription = 'badgDescription1';
//     badge1.badgExpiration = new Date();
//     badge1.badgPreuveUrl = 'badgPreuveUrl1';
//     let badge2 = new Badge();
//     badge2.badgId = '22222222222';
//     badge2.badgNom = 'badgNom2';
//     badge2.badgImageUrl = 'badgImageUrl2';
//     badge2.badgDescription = 'badgDescription2';
//     badge2.badgExpiration = new Date();
//     badge2.badgPreuveUrl = 'badgPreuveUrl2';
//     // users
//     this.bdd.users = [];
//     let user1 = new User();
//     user1.utilId = '11111111111';
//     user1.utilPseudo = 'utilPseudo1';
//     user1.utilEmail = 'utilEmail1@email.fr';
//     user1.utilPrenom = 'utilPrenom1';
//     user1.utilNom = 'utilNom1';
//     user1.utilMotDePasse = 'utilMotDePasse1';
//     user1.utilUrlAvatar = 'utilUrlAvatar1';
//     user1.utilPointDispo = 0;
//     user1.utilPointBonusDispo = 0;
//     let user2 = new User();
//     user2.utilId = '22222222222';
//     user2.utilPseudo = 'utilPseudo2';
//     user2.utilEmail = 'utilEmail2@email.fr';
//     user2.utilPrenom = 'utilPrenom2';
//     user2.utilNom = 'utilNom2';
//     user2.utilMotDePasse = 'utilMotDePasse2';
//     user2.utilUrlAvatar = 'utilUrlAvatar2';
//     user2.utilPointDispo = 0;
//     user2.utilPointBonusDispo = 0;
//     // competences
//     this.bdd.competences = [];
//     let competence1 = new Competence();
//     competence1.compId = '11111111111';
//     competence1.compNom = 'compNom1';
//     competence1.compDescription = 'compDescription1';
//     competence1.comComp = 'comComp1';
//     let competence2 = new Competence();
//     competence2.compId = '22222222222';
//     competence2.compNom = 'compNom2';
//     competence2.compDescription = 'compDescription2';
//     competence2.comComp = 'comComp2';
//     // regles
//     this.bdd.regles = [];
//     let regle1 = new Regle();
//     regle1.reglId = '11111111111';
//     regle1.reglNom = 'reglNom1';
//     regle1.reglValeur = 'reglValeur1';
//     regle1.reglType = 'reglType1';
//     let regle2 = new Regle();
//     regle2.reglId = '22222222222';
//     regle2.reglNom = 'reglNom2';
//     regle2.reglValeur = 'reglValeur2';
//     regle2.reglType = 'reglType2';
//     // typeorgas
//     this.bdd.typeorgas = [];
//     let typeorga1 = new TypeOrga();
//     typeorga1.typeorgaId = '11111111111';
//     typeorga1.typeorgaNom = 'typeorgaNom1';
//     let typeorga2 = new TypeOrga();
//     typeorga2.typeorgaId = '22222222222';
//     typeorga2.typeorgaNom = 'typeorgaNom2';
//     // organisations
//     this.bdd.organisations = [];
//     let organisation1 = new Organisation();
//     organisation1.orgaId = '11111111111';
//     organisation1.orgaNom = 'orgaNom1';
//     organisation1.orgaAdreRue = 'orgaAdreRue1';
//     organisation1.orgaAdreNumero = 'orgaAdreNumero1';
//     organisation1.orgaAdreComplement = 'orgaAdreComplement1';
//     organisation1.orgaAdreVille = 'orgaAdreVille1';
//     organisation1.orgaAdreCp = 'orgaAdreCp1';
//     organisation1.orgaAdrePays = 'orgaAdrePays1';
//     organisation1.orgaTel = 'orgaTel1';
//     organisation1.orgaMail = 'orgaMail1';
//     organisation1.orgaSiret = 'orgaSiret1';
//     organisation1.orgOrga = 'orgOrga1';
//     let organisation2 = new Organisation();
//     organisation2.orgaId = '22222222222';
//     organisation2.orgaNom = 'orgaNom2';
//     organisation2.orgaAdreRue = 'orgaAdreRue2';
//     organisation2.orgaAdreNumero = 'orgaAdreNumero2';
//     organisation2.orgaAdreComplement = 'orgaAdreComplement2';
//     organisation2.orgaAdreVille = 'orgaAdreVille2';
//     organisation2.orgaAdreCp = 'orgaAdreCp2';
//     organisation2.orgaAdrePays = 'orgaAdrePays2';
//     organisation2.orgaTel = 'orgaTel2';
//     organisation2.orgaMail = 'orgaMail2';
//     organisation2.orgaSiret = 'orgaSiret2';
//     organisation2.orgOrga = 'orgOrga2';
//     // votes
//     this.bdd.votes = [];
//     let vote1 = new Vote();
//     vote1.voteId = '11111111111';
//     vote1.voteDate = new Date();
//     vote1.voteValide = true;
//     vote1.voteType = 'voteType1';
//     let vote2 = new Vote();
//       vote2.voteId = '22222222222';
//     vote2.voteDate = new Date();
//     vote2.voteValide = false;
//     vote2.voteType = 'voteType2';
//
//     // nested entities
//     autorisation1.role = role1;
//     autorisation2.role = role2;
//     role1.auto = autorisation1;
//     role1.util = user1;
//     role2.auto = autorisation2;
//     role2.util = user2;
//     badge1.comp = competence1;
//     badge1.util = user1;
//     badge2.comp = competence2;
//     badge2.util = user2;
//     user1.roles = role1;
//     user1.badg = badge1;
//     user2.roles = role2;
//     user2.badg = badge1;
//     competence1.badg = badge1;
//     competence1.comComp = competence1;
//     competence1.orga = organisation1;
//     competence2.badg = badge2;
//     competence2.comComp = competence2;
//     competence2.orga = organisation2;
//     organisation1.regl = regle1;
//     organisation1.comp = competence1;
//     organisation1.typeorga = typeorga1;
//     organisation2.typeorga = typeorga2;
//     organisation2.regl = regle2;
//     organisation2.comp = competence2;
//     regle1.orga = organisation1;
//     regle2.orga = organisation2;
//     vote1.util = user1;
//     vote1.utiUtil = user2;
//     vote1.comp = competence1;
//     vote2.util = user1;
//     vote2.utiUtil = user2;
//     vote2.comp = competence1;
//
//     // send to bdd
//     this.bdd.badges.push(badge1);
//     this.bdd.badges.push(badge2);
//     this.bdd.autorisations.push(autorisation1);
//     this.bdd.autorisations.push(autorisation2);
//     this.bdd.roles.push(role1);
//     this.bdd.roles.push(role2);
//     this.bdd.users.push(user1);
//     this.bdd.users.push(user2);
//     this.bdd.competences.push(competence1);
//     this.bdd.competences.push(competence2);
//     this.bdd.organisations.push(organisation1);
//     this.bdd.organisations.push(organisation2);
//     this.bdd.regles.push(regle1);
//     this.bdd.regles.push(regle2);
//     this.bdd.typeorgas.push(typeorga1);
//     this.bdd.typeorgas.push(typeorga2);
//     this.bdd.votes.push(vote1);
//     this.bdd.votes.push(vote2);
//
//   }
//
//   fakeRouted(component, method) {
//     for (let fakeRoute of this.fakeRoutes) {
//       if (component.match(fakeRoute.fakeUrlPattern) && fakeRoute.method === method) {
//         return true;
//       }
//     }
//     return false;
//   }
//
//   fakeRouting(component, request) {
//     if (component.match(/.*\/v1\/[a-z]*\/all/)) {
//       return this.getAll(request);
//     } else if (component.match( /.*\/v1\/authentication\/login/)) {
//       return this.authenticate(request);
//     } else if (component.match( /.*\/v1\/authentication\/logout/)) {
//       return this.logout(request);
//     } else if (component.match( /.*\/v1\/autorisations\/role/)) {
//       return this.getAutorisationsByRole(request);
//     } else if (component.match(/.*\/v1\/[a-z]*/)) {
//       switch (request.method) {
//         case 'GET':
//           return this.getById(request);
//         case 'POST':
//           return this.postElt(request);
//         case 'PUT':
//           return this.putElt(request);
//         case 'DELETE':
//           return this.deleteElt(request);
//       }
//     }
//   }
//
//   logout(request) {
//     return of(new HttpResponse({ status: 200 }));
//   }
//
//   getAutorisationsByRole(request) {
//     let body = ['CreateUsers', 'ReadUsers', 'UpdateUsers', 'DeleteUsers',
//                 'CreateOrganisations', 'ReadOrganisations', 'UpdateOrganisations', 'DeleteOrganisations',
//                 'CreateCompetences', 'ReadCompetences', 'UpdateCompetences', 'DeleteCompetences',
//                 'CreateBadges', 'ReadBadges', 'UpdateBadges', 'DeleteBadges',
//                 'CreateRegles', 'ReadRegles', 'UpdateRegles', 'DeleteRegles',
//                 'CreateRoles', 'ReadRoles', 'UpdateRoles', 'DeleteRoles',
//                 'CreateAutorisations', 'ReadAutorisations', 'UpdateAutorisations', 'DeleteAutorisations',
//                 'ReadVotes',
//                 'CreateOrganisations', 'ReadOrganisations', 'UpdateOrganisations', 'DeleteOrganisations']
//     return of(new HttpResponse({ status: 200, body }));
//   }
//
//   authenticate(request) {
//     if (request.body.username === this.testUser.utilEmail && request.body.password === this.testUser.utilMotDePasse) {
//       // if login details are valid return 200 OK with a fake jwt token
//       let body = {
//         utilId: this.testUser.utilId,
//         utilEmail: this.testUser.utilEmail,
//         utilNom: this.testUser.utilNom,
//         utilPrenom: this.testUser.utilPrenom,
//         apiToken: 'fake-jwt-token'
//       };
//       return of(new HttpResponse({ status: 200, body }));
//     } else {
//       // else return 400 bad request
//       return throwError({ error: { message: 'Username or password is incorrect' } });
//     }
//   }
//
//   getAll(request) {
//     let collection = request.url.replace(/.*\/v1\//, '').replace(/\/all/, '');
//     let body = this.bdd[collection];
//     return of(new HttpResponse({ status: 200, body }));
//   }
//
//   getById(request) {
//     let collection = request.url.split('/').pop();
//     let id = request.params.map.get('id')[0];
//     let body = this.findById(collection, id);
//     return of(new HttpResponse({ status: 200, body }));
//   }
//
//   postElt(request) {
//     let collection = request.url.split('/').pop();
//     let elt = request.body;
//     let newId = this.currentIndexes[collection].toString().repeat(11);
//     this.currentIndexes[collection]++;
//     elt[this.eltIdField[collection]] = newId;
//     this.bdd[collection].push(elt);
//     let body = this.findById(collection, newId);
//     return of(new HttpResponse({ status: 200, body }));
//   }
//
//   putElt(request) {
//     let collection = request.url.split('/').pop();
//     let elt = request.body;
//     let eltToModify = this.bdd[collection].findIndex(e => e.id === elt.id);
//     this.bdd[collection][eltToModify] = elt;
//     let body = this.findById(collection, elt.id);
//     return of(new HttpResponse({ status: 200, body }));
//   }
//
//   deleteElt(request) {
//     let collection = request.url.split('/').pop();
//     let id = request.params.map.get('id')[0];
//     let index = this.bdd[collection].findIndex(e => e.id === id);
//     this.deleteById(collection, index);
//     let body = {};
//     return of(new HttpResponse({ status: 200, body }));
//   }
//
//
//   // "bdd" method
//   findById(collection, id) {
//     return this.bdd[collection].find(e => e[this.eltIdField[collection]] === id);
//   }
//
//   deleteById(collection, index) {
//     this.bdd[collection].splice(index, 1);
//   }
// }
//
//
