import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { JwtInterceptor } from './shared/helpers/jwt.interceptor';
import { SharedModule } from './shared/shared.module';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from './shared/auth/AuthenticationService';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { UtilisateursComponent } from './components/utilisateur/utilisateurs/utilisateurs.component';
import { RolesComponent } from './components/role/roles/roles.component';
import { AutorisationsComponent } from './components/autorisation/autorisations/autorisations.component';
import { OrganisationsComponent } from './components/organisation/organisations/organisations.component';
import { TypeorgasComponent } from './components/typeorga/typeorgas/typeorgas.component';
import { ReglesComponent } from './components/regle/regles/regles.component';
import { VotesComponent } from './components/vote/votes/votes.component';
import { CompetencesComponent } from './components/competence/competences/competences.component';
import { BadgesComponent } from './components/badge/badges/badges.component';
import { BadgeassignationsComponent } from './components/badgeassignation/badgeassignations/badgeassignations.component';

export function jwtOptionsFactory(authService) {
  return {
    tokenGetter: () => {
      return localStorage.getItem('token');
    }
  };
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    UtilisateursComponent,
    RolesComponent,
    AutorisationsComponent,
    OrganisationsComponent,
    TypeorgasComponent,
    ReglesComponent,
    VotesComponent,
    CompetencesComponent,
    BadgesComponent,
    BadgeassignationsComponent
  ],
  imports: [
    BrowserModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [AuthenticationService]
      }
    }),
    AppRoutingModule,
    CommonModule,
    NgbModule,
    SharedModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    TableModule,
    ConfirmDialogModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    ConfirmationService
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule {
}
