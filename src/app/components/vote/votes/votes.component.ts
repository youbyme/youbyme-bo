import { Component, OnInit } from '@angular/core';
import { Vote } from '../../../models/vote.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { VoteService } from '../../../shared/services/vote.service';
import { User } from '../../../models/user.model';
import { Organisation } from '../../../models/organisation.model';
import { Role } from '../../../models/role.model';
import { Competence } from '../../../models/competence.model';

@Component({
  selector: 'app-votes',
  templateUrl: './votes.component.html',
  styleUrls: ['./votes.component.scss']
})
export class VotesComponent implements OnInit {
  public currentCrud = 'votes';

  displayDialog = false;

  element: Vote = {};

  selectedElement: Vote;

  newElement: boolean;

  elements: Vote[];
  elementsTable: Vote[] = [];
  loading = false;
  tableParams;
  totalElements = 0;

  lists = {};

  cols: any[];

  constructor(
    private authenticationService: AuthenticationService,
    private voteService: VoteService
  ) {
  }

  ngOnInit() {
    // this.getData();
    this.getSelectLists();

    this.cols = [
      {field: 'voteId', header: 'Numéro'},
      {field: 'date', header: 'Date du vote'},
      {field: 'valide', header: 'Vote valide'},
      {field: 'type', header: 'Type du vote'},
      {field: 'utilisateurEmetteur', header: 'Emetteur'},
      {field: 'utilisateurReceveur', header: 'Récepteur'},
      {field: 'competence', header: 'Compétence'}
    ];
  }

  getData(tableParams?) {
    this.voteService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
      this.elementsTable = data;
      this.totalElements = this.elements.length;
      if (tableParams !== undefined) {
        this.tableParams = tableParams;
      } else {
        tableParams = this.tableParams;
      }
      if (tableParams !== undefined) {
        this.filter(tableParams);
      } else {
        this.loading = false;
      }

    });
  }

  lazyLoading(tableParams) {
    this.loading = true;
    this.getData(tableParams);
  }

  filter(tableParams) {
    // filtres
    if (tableParams.filters['voteId']) {
      this.elementsTable = this.elementsTable.filter((v: Vote) => v.voteId.match(tableParams.filters['voteId'].value));
    }
    if (tableParams.filters['date']) {
      this.elementsTable = this.elementsTable.filter((v: Vote) => v.date.toString().match(tableParams.filters['date'].value));
    }
    if (tableParams.filters['valide']) {
      this.elementsTable = this.elementsTable.filter((v: Vote) => v.valide.match(tableParams.filters['valide'].value));
    }
    if (tableParams.filters['type']) {
      this.elementsTable = this.elementsTable.filter(
        (v: Vote) => v.type.toString().match(tableParams.filters['type'].value)
      );
    }
    if (tableParams.filters['utilisateurEmetteur']) {
      const utilisateurEmeteur: Array<User> = tableParams.filters['utilisateurEmetteur'].value;
      this.elementsTable = this.elementsTable.filter((v: Vote) => utilisateurEmeteur.filter(
        (u: User) => u.utilId === v.utilisateurEmetteur.utilId
        ).length > 0
      );
    }
    if (tableParams.filters['utilisateurReceveur']) {
      const utilisateurReceveur: Array<User> = tableParams.filters['utilisateurReceveur'].value;
      this.elementsTable = this.elementsTable.filter((v: Vote) => utilisateurReceveur.filter(
        (u: User) => u.utilId === v.utilisateurReceveur.utilId
        ).length > 0
      );
    }
    if (tableParams.filters['competence']) {
      const comps: Array<Competence> = tableParams.filters['competence'].value;
      this.elementsTable = this.elementsTable.filter((v: Vote) => comps.filter(
        (c: Competence) => c.compId === v.competence.compId
        ).length > 0
      );
    }
    this.totalElements = this.elementsTable.length;
    // pagination
    this.elementsTable = this.elementsTable.slice(tableParams.first, tableParams.first + tableParams.rows);
    this.loading = false;
  }

  getSelectLists() {
    this.voteService.getSelectList('competences').subscribe((selectList: Array<any>) => {
      this.lists['competences'] = selectList;
    });
    this.voteService.getSelectList('utilisateurs').subscribe((selectList: Array<any>) => {
      this.lists['utilisateurs'] = selectList;
    });
  }

  getHeaderClass(field) {
    switch (field) {
      case 'date':
      case 'utilisateurReceveur':
      case 'competence':
        return 'ui-p-1';
      default:
        return 'ui-p-2';
    }
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    // PUT and POST don't take a date
    delete this.element.date;
    if (this.newElement) {
      this.voteService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.voteService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  validate(valid) {
    this.element.valide = valid;
    this.save();
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  delete() {
    this.voteService.delete(this.selectedElement.voteId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateVotes', 'UpdateVotes', 'DeleteVotes'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
    }
  }

  cloneElement(c: Vote): Vote {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
