import { Component, OnInit } from '@angular/core';
import { Competence } from '../../../models/competence.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { CompetenceService } from '../../../shared/services/competence.service';

@Component({
  selector: 'app-competences',
  templateUrl: './competences.component.html',
  styleUrls: ['./competences.component.scss']
})
export class CompetencesComponent implements OnInit {

  public currentCrud = 'competences';

  displayDialog = false;

  element: Competence = {};

  selectedElement: Competence;

  newElement: boolean;

  elements: Competence[];

  lists = {};

  cols: any[];

  constructor(
    private authenticationService: AuthenticationService,
    private competenceService: CompetenceService
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getSelectLists();

    this.cols = [
      {field: 'compNom', header: 'Nom'},
      {field: 'compDescription', header: 'Description'},
      {field: 'badge', header: 'Badge'},
      {field: 'competenceParent', header: 'Competence Parent'},
      {field: 'organisations', header: 'Organisations'}
    ];

  }

  getData() {
    this.competenceService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
    });
  }

  getSelectLists() {
    this.competenceService.getSelectList('badges').subscribe((selectList: Array<any>) => {
      this.lists['badges'] = selectList;
    });

    this.competenceService.getSelectList('organisations').subscribe((selectList: Array<any>) => {
      this.lists['organisations'] = selectList;
    });

    this.competenceService.getSelectList('competences').subscribe((selectList: Array<any>) => {
      this.lists['competences'] = selectList;
    });
  }

  getHeaderClass(field) {
    switch (field) {
      case 'compNom':
      case 'competenceParent':
      case 'organisations':
        return 'ui-p-1';
      default:
        return 'ui-p-2';
    }
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    if (this.newElement) {
      this.competenceService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.competenceService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  delete() {
    this.competenceService.delete(this.selectedElement.compId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateCompetences', 'UpdateCompetences', 'DeleteCompetences'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
    }
  }

  cloneElement(c: Competence): Competence {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
