import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeassignationsComponent } from './badgeassignations.component';

describe('BadgeassignationsComponent', () => {
  let component: BadgeassignationsComponent;
  let fixture: ComponentFixture<BadgeassignationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgeassignationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeassignationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
