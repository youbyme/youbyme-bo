import { Component, OnInit } from '@angular/core';
import { Badge } from '../../../models/badge.model';
import { BadgeAssignation } from '../../../models/badge-assignation.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { BadgeassignationService } from '../../../shared/services/badgeassignation.service';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-badgeassignations',
  templateUrl: './badgeassignations.component.html',
  styleUrls: ['./badgeassignations.component.scss']
})
export class BadgeassignationsComponent implements OnInit {

  public currentCrud = 'badgeassignations';

  displayDialog = false;

  element: BadgeAssignation = null;

  selectedElement: BadgeAssignation;

  newElement: boolean;
  defaultValid = [{valideValue: false, valideNom: 'non'}];

  elements: BadgeAssignation[];
  elementsTable: BadgeAssignation[];
  loading = false;
  first = true;
  tableParams;
  totalElements = 0;

  lists = {
    valide: [
      {valideValue: true, valideNom: 'oui'},
      {valideValue: false, valideNom: 'non'}
    ]
  };

  cols: any[];


  constructor(
    private authenticationService: AuthenticationService,
    private badgeassignationService: BadgeassignationService
  ) {
  }

  ngOnInit() {
    this.getSelectLists();

    this.cols = [
      {field: 'badge', header: 'Badge'},
      {field: 'utilisateur', header: 'Nom'},
      {field: 'date', header: 'Date'},
      {field: 'valide', header: 'Valide'},
    ];
  }

  getData(tableParams?) {
    this.badgeassignationService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
      this.elementsTable = data;
      this.totalElements = this.elements.length;
      if (tableParams !== undefined) {
        this.tableParams = tableParams;
      } else {
        tableParams = this.tableParams;
      }
      if (tableParams !== undefined) {
        this.filter(tableParams);
      } else {
        this.loading = false;
      }
    });
  }

  lazyLoading(tableParams) {
    this.loading = true;
    // default filter
    if (this.first) {
      tableParams.filters = {valide: {matchMode: 'equal', value: this.defaultValid}};
      this.first = false;
    }
    this.getData(tableParams);
  }

  filter(tableParams) {
    // filtres
    if (tableParams.filters['date']) {
      this.elementsTable = this.elementsTable.filter(
        (v: BadgeAssignation) => v.date.toString().match(tableParams.filters['date'].value)
      );
    }
    if (tableParams.filters['utilisateur']) {
      const utilisateur: Array<User> = tableParams.filters['utilisateur'].value;
      this.elementsTable = this.elementsTable.filter((ba: BadgeAssignation) => utilisateur.filter(
        (u: User) => u.utilId === ba.utilisateur.utilId
        ).length > 0
      );
    }
    if (tableParams.filters['badge']) {
      const badge: Array<Badge> = tableParams.filters['badge'].value;
      this.elementsTable = this.elementsTable.filter((ba: BadgeAssignation) => badge.filter(
        (b: Badge) => b.badgId === ba.badge.badgId
        ).length > 0
      );
    }
    if (tableParams.filters['valide']) {
      const valid: Array<any> = tableParams.filters['valide'].value;
      this.elementsTable = this.elementsTable.filter((ba: BadgeAssignation) => valid.filter(
        (v: any) => ba.valide === v.valideValue
        ).length > 0
      );
    }
    this.totalElements = this.elementsTable.length;
    // pagination
    this.elementsTable = this.elementsTable.slice(tableParams.first, tableParams.first + tableParams.rows);
    this.loading = false;
  }

  getSelectLists() {
    this.badgeassignationService.getSelectList('badges').subscribe((selectList: Array<any>) => {
      this.lists['badges'] = selectList;
    });
    this.badgeassignationService.getSelectList('utilisateurs').subscribe((selectList: Array<any>) => {
      this.lists['utilisateurs'] = selectList;
    });

  }

  save() {
    if (this.newElement) {
      this.badgeassignationService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.badgeassignationService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  validate() {
    this.badgeassignationService.validUserBadge(this.element.utilisateur.utilId, this.element.badge.badgId)
      .subscribe((elt) => {
        this.finishSave();
      });
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['UpdateBadgeAssignations'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
    }
  }

  cloneElement(c: BadgeAssignation): BadgeAssignation {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
