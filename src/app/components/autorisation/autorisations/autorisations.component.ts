import { Component, OnInit } from '@angular/core';
import { Autorisation } from '../../../models/autorisation.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { AutorisationService } from '../../../shared/services/autorisation.service';

@Component({
  selector: 'app-autorisations',
  templateUrl: './autorisations.component.html',
  styleUrls: ['./autorisations.component.scss']
})
export class AutorisationsComponent implements OnInit {

  public currentCrud = 'autorisations';

  displayDialog = false;

  element: Autorisation = {};

  selectedElement: Autorisation;

  newElement: boolean;

  elements: Autorisation[];

  lists = {};

  cols: any[];

  constructor(
    private authenticationService: AuthenticationService,
    private autorisationService: AutorisationService
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getSelectLists();

    this.cols = [
      {field: 'autoNom', header: 'Nom'},
      {field: 'autoDescription', header: 'Description'},
      {field: 'roles', header: 'Roles'}
    ];

  }

  getData() {
    this.autorisationService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
    });
  }

  getSelectLists() {
    this.autorisationService.getSelectList('roles').subscribe((selectList: Array<any>) => {
      this.lists['roles'] = selectList;
    });
  }

  getHeaderClass(field) {
    switch (field) {
      case 'autoNom':
      case 'roles':
        return 'ui-p-1';
      default:
        return 'ui-p-2';
    }
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    if (this.newElement) {
      this.autorisationService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.autorisationService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  delete() {
    this.autorisationService.delete(this.selectedElement.autoId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateAutorisations', 'UpdateAutorisations', 'DeleteAutorisations'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
    }
  }

  cloneElement(c: Autorisation): Autorisation {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
