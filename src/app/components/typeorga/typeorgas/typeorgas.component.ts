import { Component, OnInit } from '@angular/core';
import { TypeOrga } from '../../../models/type-orga.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { TypeorgaService } from '../../../shared/services/typeorga.service';

@Component({
  selector: 'app-typeorgas',
  templateUrl: './typeorgas.component.html',
  styleUrls: ['./typeorgas.component.scss']
})
export class TypeorgasComponent implements OnInit {

  public currentCrud = 'typeOrgas';

  displayDialog = false;

  element: TypeOrga = {};

  selectedElement: TypeOrga;

  newElement: boolean;

  elements: TypeOrga[];

  lists = {};

  cols: any[];

  constructor(
    private authenticationService: AuthenticationService,
    private typeorgaService: TypeorgaService
  ) {
  }

  ngOnInit() {
    this.getData();

    this.cols = [
      {field: 'typeOrgaNom', header: 'Nom'}
    ];

  }

  getData() {
    this.typeorgaService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
    });
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    if (this.newElement) {
      this.typeorgaService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.typeorgaService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  delete() {
    this.typeorgaService.delete(this.selectedElement.typeOrgaId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateTypeOrgas', 'UpdateTypeOrgas', 'DeleteTypeOrgas'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
    }
  }

  cloneElement(c: TypeOrga): TypeOrga {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
