import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeorgasComponent } from './typeorgas.component';

describe('TypeorgasComponent', () => {
  let component: TypeorgasComponent;
  let fixture: ComponentFixture<TypeorgasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeorgasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeorgasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
