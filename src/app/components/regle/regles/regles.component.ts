import { Component, OnInit } from '@angular/core';
import { Regle } from '../../../models/regle.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { RegleService } from '../../../shared/services/regle.service';

@Component({
  selector: 'app-regles',
  templateUrl: './regles.component.html',
  styleUrls: ['./regles.component.scss']
})
export class ReglesComponent implements OnInit {

  public currentCrud = 'regles';

  displayDialog = false;

  element: Regle = {};

  selectedElement: Regle;

  newElement: boolean;

  elements: Regle[];

  lists = {
    types: [
      {value: 'i', label: 'Nombre'},
      {value: 'b', label: 'Booléen'},
      {value: 's', label: 'Texte'}
    ],
    booleans: [
      {value: true, label: 'Vrai'},
      {value: false, label: 'Faux'}
    ]
  };

  cols: any[];

  constructor(
    private authenticationService: AuthenticationService,
    private regleService: RegleService
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getSelectLists();

    this.cols = [
      {field: 'reglNom', header: 'Nom'},
      {field: 'reglType', header: 'Type'},
      {field: 'reglValeur', header: 'Valeur'},
      {field: 'organisations', header: 'Organisations'}
    ];
  }

  getData() {
    this.regleService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
    });
  }

  getSelectLists() {
    this.regleService.getSelectList('organisations').subscribe((selectList: Array<any>) => {
      this.lists['organisations'] = selectList;
    });
  }

  getHeaderClass(field) {
    switch (field) {
      case 'reglNom':
      case 'reglValeur':
      case 'organisations':
        return 'ui-p-1';
      default:
        return 'ui-p-2';
    }
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    if (this.newElement) {
      this.regleService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.regleService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  delete() {
    this.regleService.delete(this.selectedElement.reglId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateRegles', 'UpdateRegles', 'DeleteRegles'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
    }
  }

  cloneElement(c: Regle): Regle {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
