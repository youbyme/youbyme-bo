import { Component, OnInit } from '@angular/core';
import { Organisation } from '../../../models/organisation.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { OrganisationService } from '../../../shared/services/organisation.service';

@Component({
  selector: 'app-organisations',
  templateUrl: './organisations.component.html',
  styleUrls: ['./organisations.component.scss']
})
export class OrganisationsComponent implements OnInit {
  public currentCrud = 'organisations';

  displayDialog = false;

  element: Organisation = {};

  selectedElement: Organisation;

  newElement: boolean;

  elements: Organisation[];

  lists = {};

  cols: any[];

  constructor(
    private authenticationService: AuthenticationService,
    private organisationService: OrganisationService
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getSelectLists();

    this.cols = [
      {field: 'orgaNom', header: 'Nom'},
      {field: 'orgaAdreNumero', header: 'Adresse'},
      {field: 'orgaTel', header: 'Téléphone'},
      {field: 'orgaMail', header: 'Mail'},
      {field: 'orgaCodeAnalytics', header: 'Code Analytics'}
    ];

  }

  getData() {
    this.organisationService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
    });
  }

  getSelectLists() {
    this.organisationService.getSelectList('organisations').subscribe((selectList: Array<any>) => {
      this.lists['organisations'] = selectList;
    });
    this.organisationService.getSelectList('utilisateurs').subscribe((selectList: Array<any>) => {
      this.lists['utilisateurs'] = selectList;
    });
    this.organisationService.getSelectList('competences').subscribe((selectList: Array<any>) => {
      this.lists['competences'] = selectList;
    });
    this.organisationService.getSelectList('typeorgas').subscribe((selectList: Array<any>) => {
      this.lists['typeorgas'] = selectList;
    });
    this.organisationService.getSelectList('regles').subscribe((selectList: Array<any>) => {
      this.lists['regles'] = selectList;
    });
  }

  getHeaderClass(field) {
    switch (field) {
      case 'orgaNom':
      case 'orgaCodeAnalytics':
        return 'ui-p-1';
      default:
        return 'ui-p-2';
    }
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    if (this.newElement) {
      this.organisationService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.organisationService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
  }

  delete() {
    this.organisationService.delete(this.selectedElement.orgaId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  resetVotes() {
    this.organisationService.resetPoints(this.selectedElement.orgaId).subscribe(() => {
      this.getData();
      alert('Réinitialisation des votes effectuée');
    });
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateOrganisations', 'UpdateOrganisations', 'DeleteOrganisations'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
    }
  }

  cloneElement(c: Organisation): Organisation {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
