import { Component, OnInit } from '@angular/core';
import { Badge } from '../../../models/badge.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { BadgeService } from '../../../shared/services/badge.service';

@Component({
  selector: 'app-badges',
  templateUrl: './badges.component.html',
  styleUrls: ['./badges.component.scss']
})
export class BadgesComponent implements OnInit {

  public currentCrud = 'badges';

  displayDialog = false;

  element: Badge = {};

  selectedElement: Badge;

  newElement: boolean;

  elements: Badge[];

  lists = {};

  cols: any[];

  constructor(
    private authenticationService: AuthenticationService,
    private badgeService: BadgeService
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getSelectLists();

    this.cols = [
      {field: 'badgNom', header: 'Nom'},
      {field: 'badgDescription', header: 'Description'},
      {field: 'nbPointsRequis', header: 'Nbr Points Requis'},
      {field: 'badgExpiration', header: 'Expiration'},
      {field: 'badgImageUrl', header: 'Url de l\'image'},
      // {field: 'badgPreuveUrl', header: 'Url de la preuve'},
      {field: 'competence', header: 'Compétence'}
    ];
  }

  getData() {
    this.badgeService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
    });
  }

  getSelectLists() {
    this.badgeService.getSelectList('competences').subscribe((selectList: Array<any>) => {
      this.lists['competences'] = selectList;
    });
  }

  getHeaderClass(field) {
    switch (field) {
      case 'badgNom':
      case 'nbPointsRequis':
      case 'competence':
        return 'ui-p-1';
      default:
        return 'ui-p-2';
    }
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    if (this.newElement) {
      this.badgeService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.badgeService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  delete() {
    this.badgeService.delete(this.selectedElement.badgId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateBadges', 'UpdateBadges', 'DeleteBadges'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
    }
  }

  cloneElement(c: Badge): Badge {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
