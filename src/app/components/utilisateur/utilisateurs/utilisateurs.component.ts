import { Component, OnInit } from '@angular/core';

import { User } from '../../../models/user.model';
import { Role } from '../../../models/role.model';
import { Organisation } from '../../../models/organisation.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { UtilisateurService } from '../../../shared/services/utilisateur.service';
import am4lang_fr_FR from '@amcharts/amcharts4/lang/fr_FR';

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-utilisateurs',
  templateUrl: './utilisateurs.component.html',
  styleUrls: ['./utilisateurs.component.scss']
})
export class UtilisateursComponent implements OnInit {
  public currentCrud = 'utilisateurs';

  displayDialog = false;
  pwdVisible = 'password';
  pwdIcon = 'fa-eye';

  element: User = {};

  selectedElement: User;

  newElement: boolean;

  elements: User[];
  elementsTable: User[] = [];
  totalElements = 0;
  userProgresses;

  lists = {civilite: [{label: 'Monsieur', value: 'm'}, {label: 'Madame', value: 'mme'}]};

  cols: any[];

  dataRoles = [];

  pointFilter: number;

  chart;
  loadingImport = false;
  radarDisplayed = false;
  loading = false;
  tableParams;

  constructor(
    private authenticationService: AuthenticationService,
    private utilisateurService: UtilisateurService
  ) {
  }

  ngOnInit() {
    // this.getData();
    this.getSelectLists();

    this.cols = [
      {field: 'utilPseudo', header: 'Pseudo'},
      {field: 'utilEmail', header: 'Mail'},
      {field: 'utilPrenom', header: 'Prénom'},
      {field: 'utilNom', header: 'Nom'},
      {field: 'utilPointDispo', header: 'Points dispos'},
      {field: 'organisationsGeres', header: 'Organisations Gérées'},
      {field: 'utilRoles', header: 'Role'},
      {field: 'organisations', header: 'Organisation'}
    ];
  }

  getData(tableParams?) {
    this.utilisateurService.getAll().subscribe((data: Array<any>) => {
      this.dataRoles = [...data];
      data.map((elt, eltIndex, eltArray) => {
        elt.utilRoles.map((sub, subIndex, subArray) => {
          delete this.dataRoles[eltIndex].utilRoles[subIndex].autorisations;
        });
      });
      this.elements = this.dataRoles;
      this.elementsTable = this.dataRoles;
      this.totalElements = this.elements.length;
      if (tableParams !== undefined) {
        this.tableParams = tableParams;
      } else {
        tableParams = this.tableParams;
      }
      if (tableParams !== undefined) {
        this.filter(tableParams);
      } else {
        this.loading = false;
      }
    });
  }

  lazyLoading(tableParams) {
    this.loading = true;
    this.getData(tableParams);
  }

  filter(tableParams) {
    // filtres
    if (tableParams.filters['utilPseudo']) {
      this.elementsTable = this.elementsTable.filter((u: User) => u.utilPseudo.match(tableParams.filters['utilPseudo'].value));
    }
    if (tableParams.filters['utilEmail']) {
      this.elementsTable = this.elementsTable.filter((u: User) => u.utilEmail.match(tableParams.filters['utilEmail'].value));
    }
    if (tableParams.filters['utilPrenom']) {
      this.elementsTable = this.elementsTable.filter((u: User) => u.utilPrenom.match(tableParams.filters['utilPrenom'].value));
    }
    if (tableParams.filters['utilNom']) {
      this.elementsTable = this.elementsTable.filter((u: User) => u.utilNom.match(tableParams.filters['utilNom'].value));
    }
    if (tableParams.filters['utilPointDispo']) {
      this.elementsTable = this.elementsTable.filter(
        (u: User) => u.utilPointDispo.toString().match(tableParams.filters['utilPointDispo'].value)
      );
    }
    if (tableParams.filters['organisationsGeres']) {
      const orgaGerees: Array<Organisation> = tableParams.filters['organisationsGeres'].value;
      this.elementsTable = this.elementsTable.filter(
        (u: User) => orgaGerees.filter(
          (og: Organisation) => u.organisationsGeres.filter(
            (o: Organisation) => o.orgaId === og.orgaId
          ).length > 0
        ).length > 0
      );
    }
    if (tableParams.filters['utilRoles']) {
      const roles: Array<Role> = tableParams.filters['utilRoles'].value;
      this.elementsTable = this.elementsTable.filter(
        (u: User) => roles.filter(
          (sr: Role) => u.utilRoles.filter(
            (r: Role) => r.roleId === sr.roleId
          ).length > 0
        ).length > 0
      );
    }
    if (tableParams.filters['organisations']) {
      const orgas: Array<Organisation> = tableParams.filters['organisations'].value;
      this.elementsTable = this.elementsTable.filter((u: User) => orgas.filter(
        (og: Organisation) => u.organisation.orgaId === og.orgaId
        ).length > 0
      );
    }
    this.totalElements = this.elementsTable.length;
    // pagination
    this.elementsTable = this.elementsTable.slice(tableParams.first, tableParams.first + tableParams.rows);
    this.loading = false;
  }

  getSelectLists() {
    this.utilisateurService.getSelectList('roles').subscribe((selectList: Array<any>) => {
      this.lists['roles'] = selectList;
    });
    this.utilisateurService.getSelectList('organisations').subscribe((selectList: Array<any>) => {
      this.lists['organisations'] = selectList;
    });
  }

  getHeaderClass(field) {
    switch (field) {
      case 'utilNom':
      case 'organisations':
      case 'utilPrenom':
        return 'ui-p-1';
      default:
        return 'ui-p-2';
    }
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    if (this.newElement) {
      this.utilisateurService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.utilisateurService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  delete() {
    this.utilisateurService.delete(this.selectedElement.utilId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateUtilisateurs', 'UpdateUtilisateurs', 'DeleteUtilisateurs'])) {
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
      this.newElement = false;
      this.utilisateurService.getProgresses(this.element.utilId).subscribe((progresses) => {
        this.userProgresses = progresses;
        this.createUserRadarChart(this.userProgresses);
      });
    }
  }

  cloneElement(c: User): User {
    const element = {};
    for (const prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }

  importUsers(event) {
    let file = event.target.files[0];
    let reader = new FileReader();
    // Read file into memory as UTF-8
    reader.readAsText(file);
    // Handle errors load
    reader.onload = this.processData;
    reader.onerror = this.errorHandler;
  }

  processData = (event) => {
    const csv = event.target.result;
    const allTextLines = csv.split(/\r\n|\n/);
    const users = {};
    let user;
    for (let i = 0; i < allTextLines.length; i++) {
      let data = allTextLines[i].split(';');
      if (data[0] !== 'Pseudo' && data[0] !== '') {
        if (!users[data[8]]) {
          users[data[8]] = [];
        }
        user = new User();
        user.utilPseudo = data[0];
        user.utilEmail = data[1];
        user.utilCivilite = data[2];
        user.utilPrenom = data[3];
        user.utilNom = data[4];
        user.utilPointDispo = parseInt(data[5]);
        user.utilPointBonusDispo = parseInt(data[6]);
        user.utilMotDePasse = data[7];
        users[data[8]].push(user);
      }
    }
    for (const codeAnalytics in users) {
      this.utilisateurService.postUsersList(users[codeAnalytics], codeAnalytics)
        .subscribe((elt) => {
            this.getData();
          },
          error => {
            console.error(error);
          });
    }
  };

  showPassword(visible) {
    this.pwdVisible = visible ? 'text' : 'password';
    this.pwdIcon = visible ? 'fa-eye-slash' : 'fa-eye';
  }

  getUserUrlAvatar() {
    return this.element.utilUrlAvatar ? this.element.utilUrlAvatar : 'assets/images/avatar.png';
  }

  errorHandler(evt) {
    if (evt.target.error.name === 'NotReadableError') {
      alert('Canno\'t read file !');
    }
  }

  createUserRadarChart(datas, target?) {
    if (!target) {
      target = 'radarchartdiv';
    }
    am4core.useTheme(am4themes_animated);
    // Themes end

    /* Create chart instance */
    this.chart = am4core.create(target, am4charts.RadarChart);
    this.chart.language.locale = am4lang_fr_FR;

    /* Add data */
    this.chart.data = datas;

    /* Create axes */
    const categoryAxis = this.chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'competence';

    const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.axisFills.template.fill = this.chart.colors.getIndex(2);
    valueAxis.renderer.axisFills.template.fillOpacity = 0.05;

    /* Create and configure series */
    const series = this.chart.series.push(new am4charts.RadarSeries());
    series.dataFields.valueY = 'percentage';
    series.dataFields.categoryX = 'competence';
    series.name = 'Sales';
    series.strokeWidth = 3;
  }

  deployRadar(visible?) {
    if (visible === undefined) {
      visible = !this.radarDisplayed;
    }
    this.radarDisplayed = visible;
    if (this.radarDisplayed) {
      setTimeout(() => {
        this.createUserRadarChart(this.userProgresses, 'radarChartFull');
      }, 100);
    }
  }
}
