import { Component, OnInit } from '@angular/core';
import { Role } from '../../../models/role.model';
import { AuthenticationService } from '../../../shared/auth/AuthenticationService';
import { RoleService } from '../../../shared/services/role.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  public currentCrud = 'roles';

  displayDialog = false;

  element: Role = {};

  selectedElement: Role;

  newElement: boolean;

  elements: Role[];

  lists = {};

  cols: any[];

  constructor(
    private authenticationService: AuthenticationService,
    private roleService: RoleService
  ) {
  }

  ngOnInit() {
    this.getData();
    this.getSelectLists();

    this.cols = [
      {field: 'roleNom', header: 'Nom'},
      {field: 'roleDescription', header: 'Description'}
    ];

  }

  getData() {
    this.roleService.getAll().subscribe((data: Array<any>) => {
      this.elements = data;
    });
  }

  getSelectLists() {
    this.roleService.getSelectList('autorisations').subscribe((selectList: Array<any>) => {
      this.lists['autorisations'] = selectList;
    });
    this.roleService.getSelectList('utilisateurs').subscribe((selectList: Array<any>) => {
      this.lists['utilisateurs'] = selectList;
    });
  }

  showDialogToAdd() {
    this.newElement = true;
    this.element = {};
    this.displayDialog = true;
  }

  save() {
    if (this.newElement) {
      this.roleService.post(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    } else {
      this.roleService.put(this.element)
        .subscribe((elt) => {
          this.finishSave();
        });
    }
  }

  finishSave() {
    this.getData();
    this.element = null;
    this.displayDialog = false;
    alert('Modification effectuées avec succès');
  }

  delete() {
    this.roleService.delete(this.selectedElement.roleId).subscribe(() => {
      this.getData();
    });
    this.displayDialog = false;
  }

  onRowSelect(event) {
    if (this.authenticationService.hasOneFeature(['CreateRoles', 'UpdateRoles', 'DeleteRoles'])) {
      this.newElement = false;
      this.element = this.cloneElement(event.data);
      this.displayDialog = true;
      console.log(this.element);
      console.log(this.lists['autorisations']);
    }
  }

  JSONstringify(elt) {
    return JSON.stringify(elt);
  }

  cloneElement(c: Role): Role {
    let element = {};
    for (let prop in c) {
      element[prop] = c[prop];
    }
    return element;
  }
}
