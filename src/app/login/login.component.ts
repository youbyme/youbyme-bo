import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../shared/auth/AuthenticationService';
import { AutorisationService } from '../shared/services/autorisation.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private autorisationService: AutorisationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.route.params.subscribe(val => {
      // put the code from `ngOnInit` here
      this.ngOnInit();
    });
  }

  ngOnInit() {
    if (this.route.snapshot.data['logout']) {
      this.authenticationService.logout();
    }
    this.error = '';
    this.loading = false;

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from component parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        tokens => {
          this.authenticationService.getConnectedUser().subscribe((user: User) => {
            // TODO check if it still usefull
            user.token = tokens.token;
            user.refresh_token = tokens.refresh_token;
            // FIN TODO

            // transform auth list from role to string array to make utilisation easier
            user.features = [];
            user.features.push('loggued');
            for (const role of user.utilRoles) {
              for (const feat of role.autorisations) {
                user.features.push(feat.autoNom);
              }
            }
            // test if user can use the back office
            if (this.authenticationService.hasOneFeature(['UseBO'])) {
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              this.authenticationService.setCurrentUser(user);
              this.router.navigate([this.returnUrl]).then(() => {
                this.loading = false;
                return user;
              });
            } else {
              alert('Vous n\'avez pas les autorisations nécessaires pour vous connecter');
              this.loading = false;
            }
          },
          error => {
            this.error = error.error.message;
            this.loading = false;
          });
        },
        error => {
          this.error = error.error.message;
          this.loading = false;
        });
  }
}
