export class Badge {
  constructor(
    public badgId?,
    public badgNom?,
    public badgImageUrl?,
    public badgDescription?,
    public badgExpiration?,
    public badgPreuveUrl?,
    public nbPointsRequis?,
    public competence?,
    public attributionsUtilisateur?
  ) {
  }
}
