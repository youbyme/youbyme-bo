export class Competence {
  constructor(
    public compId?,
    public compNom?,
    public compDescription?,
    public competenceParent?,
    public organisations?,
    public badge?
  ) {
    this.competenceParent = {};
  }
}

