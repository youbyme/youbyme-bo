export class Role {
  constructor(
    public roleId?,
    public roleNom?,
    public roleDescription?,
    public autorisations?,
    public utilisateurs?
  ) {
  }
}
