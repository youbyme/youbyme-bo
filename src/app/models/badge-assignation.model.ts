export class BadgeAssignation {
  constructor(
    public utilisateur?,
    public badge?,
    public date?: Date,
    public valide?: boolean
  ) {
  }
}
