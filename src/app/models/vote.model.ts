export class Vote {
  constructor(
    public voteId?,
    public date?: Date,
    public valide?,
    public type?,
    public utilisateurEmetteur?,
    public utilisateurReceveur?,
    public competence?
  ) {
  }
}

export class SendableVote {
  constructor(
    public voteId?,
    public valide?,
    public type?,
    public utilisateurEmetteur?,
    public utilisateurReceveur?,
    public competence?
  ) {
  }
}
