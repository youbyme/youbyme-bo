export class Regle {
  constructor(
    public reglId?,
    public reglNom?,
    public reglValeur?,
    public reglType?,
    public organisations?
  ) {
  }
}
