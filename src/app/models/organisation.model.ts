export class Organisation {
  constructor(
    public orgaId?,
    public orgaNom?,
    public orgaCodeAnalytics?,
    public orgaAdreRue?,
    public orgaAdreNumero?,
    public orgaAdreComplement?,
    public orgaAdreVille?,
    public orgaAdreCp?,
    public orgaAdrePays?,
    public orgaTel?,
    public orgaMail?,
    public orgaSiret?,
    public orgaParent?,
    public typeOrga?,
    public regles?,
    public competences?,
    public utilisateurs?,
    public administrateurs?
  ) {
  }
}
