export class Autorisation {
  constructor(
    public autoId?,
    public autoNom?,
    public autoDescription?,
    public roles?
  ) {
  }
}
