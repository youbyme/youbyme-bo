export class User {
  constructor(
    public utilId?,
    public utilPseudo?,
    public utilEmail?,
    public utilCivilite?,
    public utilPrenom?,
    public utilNom?,
    public utilMotDePasse?,
    public utilSalt?,
    public utilUrlAvatar?,
    public utilPointDispo?,
    public utilPointBonusDispo?,
    public utilRoles?,
    public organisation?,
    public badges?,
    public organisationsGeres?,
    public password?,
    public salt?,
    public username?,
    public roles?,
    // only for front
    public features?: string[],
    public token?,
    public refresh_token?,
  ) {}
}
