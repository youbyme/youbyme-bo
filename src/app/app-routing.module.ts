import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './shared/auth/auth.gard';
import { AutorisationsComponent } from './components/autorisation/autorisations/autorisations.component';
import { BadgesComponent } from './components/badge/badges/badges.component';
import { BadgeassignationsComponent } from './components/badgeassignation/badgeassignations/badgeassignations.component';
import { CompetencesComponent } from './components/competence/competences/competences.component';
import { OrganisationsComponent } from './components/organisation/organisations/organisations.component';
import { ReglesComponent } from './components/regle/regles/regles.component';
import { RolesComponent } from './components/role/roles/roles.component';
import { TypeorgasComponent } from './components/typeorga/typeorgas/typeorgas.component';
import { UtilisateursComponent } from './components/utilisateur/utilisateurs/utilisateurs.component';
import { VotesComponent } from './components/vote/votes/votes.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'loggued'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {logout: false}
  },
  {
    path: 'logout',
    component: LoginComponent,
    data: {logout: true}
  },
  {
    path: 'autorisations',
    component: AutorisationsComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadAutorisations'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'badges',
    component: BadgesComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadBadges'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'badgeassignations',
    component: BadgeassignationsComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadBadgeassignations'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'competences',
    component: CompetencesComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadCompetences'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'organisations',
    component: OrganisationsComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadOrganisations'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'regles',
    component: ReglesComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadRegles'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'roles',
    component: RolesComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadRoles'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'typeorgas',
    component: TypeorgasComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadTypeorgas'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'utilisateurs',
    component: UtilisateursComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadUtilisateurs'},
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'votes',
    component: VotesComponent,
    canActivate: [AuthGuard],
    data: {authorisations: 'ReadVotes'},
    runGuardsAndResolvers: 'always'
  },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes, {
      onSameUrlNavigation: 'reload',
      useHash: true
    }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
