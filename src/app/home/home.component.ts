import { Component, OnInit, NgZone } from '@angular/core';
import { User } from '../models/user.model';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ErrorService } from '../shared/services/error.service';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4lang_fr_FR from "@amcharts/amcharts4/lang/fr_FR";
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { formatDate, registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { UtilisateurService } from '../shared/services/utilisateur.service';
import { BadgeassignationService } from '../shared/services/badgeassignation.service';
import { BadgeAssignation } from '../models/badge-assignation.model';
import frLocale from '@fullcalendar/core/locales/fr';

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private chart: am4charts.XYChart;

  constructor(
    private errorService: ErrorService,
    private http: HttpClient,
    private badgeassignationService: BadgeassignationService,
    private utilisateurService: UtilisateurService,
    private zone: NgZone
  ) {
    registerLocaleData(localeFr, 'fr-FR', localeFrExtra);
  }

  elements: User[];

  userCount: number;
  voteCount: number;
  badgeCount: number;
  currUsername: String;
  currDate;

  events;
  options;
  calendarExpanded = false;

  ngOnInit() {
    this.getData();
    let currUser = JSON.parse(localStorage.getItem('currentUser'));
    this.currUsername = currUser['utilPrenom'] + ' ' + currUser['utilNom'];
    this.currDate = formatDate(Date(), 'EEEE d MMMM y', 'fr-FR');
    this.utilisateurService.getUserResetHistory().subscribe((history) => {
      this.events = this.formatHistory(history);
    });
    this.options = {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      locale: frLocale
    };
  }

  toggleCalendar() {
    this.calendarExpanded = !this.calendarExpanded;
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  createChart(data) {
    this.chart = am4core.create('chartdiv', am4charts.XYChart);
    this.chart.language.locale = am4lang_fr_FR;
    this.chart.paddingRight = 20;

    this.chart.data = data;

    let dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.grid.template.location = 0;

    let valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 35;

    let series = this.chart.series.push(new am4charts.LineSeries());
    series.dataFields.dateX = 'date';
    series.dataFields.valueY = 'value';

    series.tooltipText = '{valueY.value}';
    this.chart.cursor = new am4charts.XYCursor();

  }

  getUsersOfAdmin() {
    this.utilisateurService.getAll().subscribe((data: Array<any>) => {
      this.userCount = data.length;
    });
  }

  getBadgesOfAdmin() {
    this.badgeassignationService.getAll().subscribe((assignationBadge: Array<BadgeAssignation>) => {
      this.badgeCount = assignationBadge.filter(ba => ba.valide === false).length;
    });
  }

  getLastMonday(d) {
    let date = new Date(d);
    if (date.getDay() !== 1) {
      for (let i = 0; i < 7; i++) {
        date.setDate(date.getDate() - 1);
        if (date.getDay() === 1) {
          break;
        }
      }
    }
    return date.toJSON().substr(0, 10);
  }

  getNextSunday(d) {
    let date = new Date(d);
    for (let i = 0; i < 7; i++) {
      date.setDate(date.getDate() + 1);
      if (date.getDay() === 0) {
        break;
      }
    }
    return date.toJSON().substr(0, 10);
  }

  getSaturday(d: Date) {
    let date = new Date(d);
    for (let i = 0; i < 7; i++) {
      date.setDate(date.getDate() + 1);
      if (date.getDay() === 6) {
        return date;
      }
    }
    return d;
  }

  getVotesOfAdmin() {
    // Initialize Params Object
    let params = new HttpParams();

    // Begin assigning parameters
    params = params.append('startDate', this.getLastMonday(new Date()));
    params = params.append('endDate', this.getNextSunday(new Date()));

    let voteData = this.http.get(this.getComponentUrl('votes'), {params: params}).pipe(
      catchError(this.errorService.manageError),
      map(eltArray => this.mapAllElements(eltArray))
    );
    voteData.subscribe((votes: Array<any>) => {
      this.voteCount = votes.length;

      // group by date
      let VotesByDate = {};
      for (let vote of votes) {
        if (!VotesByDate[vote.date]) {
          VotesByDate[vote.date] = [];
        }
        VotesByDate[vote.date].push(vote);
      }
      // format data for chart
      let data = [];
      for (let dateVotes in VotesByDate) {
        data.push({
          date: new Date(dateVotes),
          name: dateVotes,
          value: VotesByDate[dateVotes].length
        });
      }

      data.sort((a, b) => {
        const date1 = new Date(a.date);
        const date2 = new Date(b.date);
        if (date1 > date2) {
          return 1;
        }
        if (date1 < date2) {
          return -1;
        }
        return 0;
      });

      this.createChart(data);
    });
  }

  date_sort_asc = function (date1: Date, date2: Date) {
    // This is a comparison function that will result in dates being sorted in
    // ASCENDING order. As you can see, JavaScript's native comparison operators
    // can be used to compare dates. This was news to me.
    if (date1 > date2) return 1;
    if (date1 < date2) return -1;
    return 0;
  };

  date_sort_desc = function (date1, date2) {
    // This is a comparison function that will result in dates being sorted in
    // DESCENDING order.
    if (date1 > date2) return -1;
    if (date1 < date2) return 1;
    return 0;
  };

  mapElement(elt) {
    return elt;
  }

  mapAllElements(eltArray) {
    for (let elt of eltArray) {
      elt = this.mapElement(elt);
    }
    return eltArray;
  }

  getComponentUrl(component, params = '') {
    return environment.apiUrl + '/api/' + environment.apiVersion + '/' + component.toLowerCase() + (params ? params : '');
  }

  getData() {
    this.getUsersOfAdmin();
    this.getVotesOfAdmin();
    this.getBadgesOfAdmin();
  }

  formatHistory(history) {
    const events = [];
    let startDate;
    for (const histo of history) {
      startDate = new Date(histo.date);
      events.push({
        title: `${histo.organisation.orgaNom} (${histo.organisation.orgaCodeAnalytics})`,
        start: startDate,
        end: this.getSaturday(startDate)
      });
    }
    return events;
  }
}
